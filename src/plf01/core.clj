(ns prueba.core)

; Función <
(defn función-<-1
  [n]
  (< n 0))

(defn función-<-2
  [a b]
  (< a b))

(defn función-<-3
  [a b c]
  (< a b c))

(función-<-1 -3)
(función-<-2 95 5)
(función-<-3 70 10 35)

; Función <=
(defn función-<=-1
  [a b]
  (<= a b))

(defn función-<=-2
  [a b c]
  (<= a b c))

(defn funcion-<=-3
  [a b c d]
  (<= a b c d))

(función-<=-1 5 7 )
(función-<=-2 9 10 9)
(funcion-<=-3 5 0 20 50)

; Función ==
(defn función-==-1
  [a b]
  (== a b))

(defn función-==-2
  [a b c]
  (== a b c))

(defn función-==-3
  [a b c d]
  (== a b c d))

(función-==-1 10 10)
(función-==-2 100 100 100)
(función-==-3 50 60 70 90)

; Funcion >
(defn funcion->-1
  [a b]
  (> a b))

(defn funcion->-2
  [a b c]
  (> a b c))

(defn funcion->-3
  [a b c d]
  (> a b c d))

(funcion->-1 20 10)
(funcion->-2 5 9 7)
(funcion->-3 4 9 7 0)

; Funcion >=
(defn funcion->=-1
  [a b]
  (>= a b))

(defn funcion->=-2
  [a b c d e]
  (>= a b c d e))

(defn funcion->=-3
  [a b d]
  (>= b a d))

(funcion->=-1 3 2)
(funcion->=-2 1 2 3 4 5)
(funcion->=-3 908 965 65)

; Funcion Assoc
(defn función-Asoc-1
  [map key val]
  (assoc map key val))

(defn función-Asoc-2
  [map key val]
  (assoc map key val))

(defn función-Asoc-3
  [map key val]
  (assoc map key val))

(función-Asoc-1 [\o \s \c \a] 4 \r )
(función-Asoc-2 nil 0 4)
(función-Asoc-3 {} 0 "Hola")

; Funcion con Concat
(defn función-concat-1
  [a b]
  concat [a b])

(defn función-concat-2
  [a b c]
  (concat a b c))

(defn función-concat-3
  [a b c d]
  (concat a b c d))

(función-concat-1 10 20)
(función-concat-2 [10] [20] [30]) 
(función-concat-3 [40 50] [90 10] [1 6] [9 0])

; Funcion Conj
(defn función-Conj-1
  [collection value]
  (conj collection value))

(defn función-Conj-2
  [collection val1 val2]
  (conj collection val1 val2))

(defn función-Conj-3
  [collection element]
  (conj collection element))

(función-Conj-1 [\a \b \c \d] \e)
(función-Conj-2 [1 2 3 9 0] 65 45)
(función-Conj-3 [1 2 1 46 6] [7 9 0])

;Funcion con Cons
(defn función-cons-1
  [valor element]
  (cons valor element))

(defn función-cons-2
  [valor element]
  (cons valor element))

(defn función-cons-3
  [valor element]
  (cons valor element))

(función-cons-1 10 '(1 5 9 6 7))
(función-cons-2 [1 2] [3 4 5 6 9 true])
(función-cons-3 "Hola" ["Como" "Estas"])

;Funcion con Contains
(defn función-contains-1
  [valor element]
  (contains? element valor))

(defn función-contains-2
  [valor element]
  (contains? element valor))

(defn función-contains-3
  [valor element]
  (contains? element valor))

(función-contains-1 10 [90 2 7 6 6 10])
(función-contains-2 \b #{\a \w \x})
(función-contains-3 true ["Hola" false 1000])
;--------------------------------Fin primer Commit----------------------------
;--------------------------------Segundo Commit------------------------------------
;funcion con Count
(defn función-count-1
  [elements]
  (count elements))

(defn función-count-2
  [elements]
  (count elements))

(defn función-count-3
  [elements]
  (count elements))

(función-count-1 "Aloooo! Me oyen ")
(función-count-2 [1 2 9 3802 true \w \@ "Mama"])
(función-count-3 {:17161210 "Oscar" :17161158 "Sergio"})

; Funcion con disj
(defn función-disj-1
  [collection n]
  (disj collection n))
(defn función-disj-2
  [collection a b]
  (disj collection a b))
(defn función-disj-3
  [collection]
  (disj collection))

(función-disj-1 #{"Arre" "Borreguito" "Vamos"} "Borreguito" )
(función-disj-2 #{1 9 10 7} 9 10)
(función-disj-3 [1 2 true "Bright"])

;funcion con Dissoc
(defn funcion-dissoc-1
  [collection]
  (dissoc collection ))

(defn funcion-dissoc-2
  [collection key]
  (dissoc collection key))

(defn funcion-dissoc-3
  [collection key key2]
  (dissoc collection key key2))

(funcion-dissoc-1 {:17161210 "Oscar" :17161170 "Jennifer"})
(funcion-dissoc-2 {:17161210 "Oscar" :16161358 "Anselmo"} :16161358)
(funcion-dissoc-3 {:17161210 "Oscar" :16161358 "Anselmo" :17161160 "Jesus"} :16161358 :17161210)


;funcions distinct
(defn función-distinct-1
  [elements]
  (distinct elements))

(defn función-distinct-2
  [elements]
  (distinct elements))

(defn función-distinct-3
  [elements]
  (distinct elements))

(función-distinct-1 [1 2 7 2 4 5 6])
(función-distinct-2 "ALOOOO!")
(función-distinct-3 '(\b \b \z \y \x))

;funcion con distinct?
(defn función-distinct?-1
  [a b]
  (distinct? a b))

(defn función-distinct?-2
  [a b c]
  (distinct? a b c))

(defn función-distinct?-3
  [a b c d]
  (distinct? a b c d))

(función-distinct?-1 1 1)
(función-distinct?-2 1 2 3 )
(función-distinct?-3 11 11 34 2)
;---------------------------------------------Fin Segundo Commit---------------------------------------
;---------------------------------------------Inicia el Tercer Commit----------------------------------------
;funcion drop-last
(defn función-drop-last-1
  [c]
  (drop-last c))

(defn función-drop-last-2
  [n c]
  (drop-last n c))

(defn función-drop-last-3
  [n c]
  (drop-last n c))

(función-drop-last-1 #{8 9 1 6 7 5 0})
(función-drop-last-2 5 [1 9 8 9 2 9 4 36 7])
(función-drop-last-3 4 '(5 4 3 1 0) )

;funcion Empty
(defn función-empty-1
  [c]
  (empty c))

(defn función-empty-2
  [c]
  (empty c))

(defn función-empty-3
  [c]
  (empty c ))

(función-empty-1 [\a \b \c \d])
(función-empty-2 '(1 2 3 4))
(función-empty-3 [1 2 3 4 5 6 7 8 9 0 0 9 8 7 6 5])

; Funcion empty?  c es una collecion
(defn función-empty-?-1
  [c]
  (empty? c))
(defn función-empty-?-2
  [c]
  (empty? c))
(defn función-empty-?-3
  [c]
  (empty? c))

(función-empty-?-1 [])
(función-empty-?-2 #{})
(función-empty-?-3 "F")

;funcion false?
(defn función-false-?-1
  [a]
  (false? a))
(defn función-false-?-2
  [a]
  (false? a))
(defn función-false-?-3
  [a]
  (false? a))
(función-false-?-1 "foo")
(función-false-?-1 nil)
(función-false-?-1 true)
;----------------------------------------Fin tercer commit----------------------------------------
;----------------------------------------Inicia Cuarto Commit-----------------------------------
;funcion find (buscar)
(defn función-find-1
  [c e]
  (find c e))
;funcion find (buscar)
(defn función-find-2
  [c e]
  (find c e))
;funcion find (buscar)
(defn función-find-3
  [c e]
  (find c e))
(función-find-1 {:17161158 "Sergio" :17161170 "Jennifer" :17161210 "Oscar"} :17161210)
(función-find-2 {:17161158 "Sergio" :17161170 "Jennifer" :17161210 "Oscar"} :17161211)
(función-find-3 {:17161158 "Sergio" :17161170 "Jennifer" :17161210 nil}     :17161210)

;funcion con first
(defn función-first-1
  [a]
  (first a))
(defn función-first-2
  [a]
  (first a))
(defn función-first-3
  [a]
  (first a))

(función-first-1 [999 998 997 1221])
(función-first-1 "Test")
(función-first-1 #{:a 17 :b 78 :c 22 :d true})

;funcion flantten
(defn función-flatten-1
  [e]
  (flatten e))

(defn función-flatten-2
  [e ]
  (flatten e))

(defn función-flatten-3
  [e]
  (flatten e ))

(función-flatten-1 7)
(función-flatten-2 [[5 4 2] [9 7 6]])
(función-flatten-3 [[false true nil] '("Cuando" "lleguen" "los" "humanos")])

;funcion frecuencias
(defn función-frequencies-1
  [c]
  (frequencies  c))
(defn función-frequencies-2
  [c]
  (frequencies c))
(defn función-frequencies-3
  [c]
  (frequencies c))

(función-frequencies-1 '(12 13 21 1 1 1 1 11 1 11 2 0 0 0 0 98))
(función-frequencies-2 [\a \a \a \b \d \f \f \b 4 2 1 true true true])
(función-frequencies-3 "ALOOOO! Siiii")

;funcion Get
(defn función-get-1
  [c e]
  (get c e))
(defn función-get-2
  [c e]
  (get c e))
(defn función-get-3
  [c e e2]
  (get c e e2))

(función-get-1 [1 2 3] 1)
(función-get-2 [1 2 3] 5)
(función-get-3 [1 2 3] 5 100)
;------------------------------------------Fin cuarto Commit-------------------------------------
;------------------------------------------Inicio Quinto Commit---------------------------------
;funcion Get-In
(defn función-get-in-1
  [c e]
  (get-in c e))

(defn función-get-in-2
  [c e]
  (get-in c e))

(defn función-get-in-3
  [c e]
  (get-in c e))

(función-get-in-1 [[4 5 3] 
                   [1 2 4] 
                   [0 2 4]] [0 2])
(función-get-in-2 [[true 5 false]
                   [false 2 [4 5 2]]] [1 2])
(función-get-in-3 [['(1 2 3 2) {:a 50 :b 60}]] [0 1])

;funcion into
(defn función-into-1
  [a b]
  (into a b))

(defn función-into-2
  [a b ]
  (into a b))

(defn función-into-3
  [a b]
  (into a b ))

(función-into-1 [1 2 3] '(4 5 6))
(función-into-2 '(40 97 0) '(4 5 6) )
(función-into-3 [1000 889 226] nil)

;funcion key
(defn función-Key-1
  [c]
  (map key c))

(defn función-Key-2
  [c]
  (map key c))
(defn función-Key-3
  [c]
  (map key c))

(función-Key-1 {:a [12 120 102] :c '(\a \b \c)})
(función-Key-2 {:17161158 "Sergio" :17161210 "Alexis" :17161170 "Jennifer"})
(función-Key-3 {:f {:g "H" :h "G"} :z '("HOLA")})

(defn función-keys-1
  [a]
  (keys a)
)
(defn función-keys-2
  [a]
  (keys a))
(defn función-keys-3
  [a]
  (keys a))

(función-keys-1 {:155558 :true, :not :join})
(función-keys-2 {:17161158 :and, :like :word})
(función-keys-3 {:171 :www, :45780 :key})

;funcion max
(defn función-max-1
  [c d]
  (max c d))

(defn función-max-2
  [a b c]
  (max a b c ))

(defn función-max-3
  [a b c d e ]
  (max a b c d e))

(función-max-1 80)
(función-max-2 98 120 0)
(función-max-3 10 70 90 1000 667)
;-----------------------------------------Fin Quinto Commit-------------------------------------
;-----------------------------------------Inicio Sexto Commit------------------------------------
;funcion merge
(defn función-merge-1
  [a b]
  (merge a b))

(defn función-merge-2
  [a b]
  (merge a b))

(defn función-merge-3
  [a b]
  (merge a b))

(función-merge-1  {:a 1 :b 2 :c 3} {:b 9 :d 4})
(función-merge-2 [8 90 10] {:b "HELLO" :d "HOLA"})
(función-merge-3 '(4 90 10 60) {:b 9 :d 4})

;funcion min
(defn función-min-1
  [a b c d]
  (min a b c d))

(defn función-min-2
  [a b c]
  (min a b c))

(defn función-min-3
  [a b c d e f]
  (min a b c d e f))

(función-min-1 50 10 60 70)
(función-min-2 2 30 10)
(función-min-3 -10 908 20 6 8 0)

;funcion neg?
(defn función-neg?-1
  [n]
  (neg? n))

(defn función-neg?-2
  [n]
  (neg? n))

(defn función-neg?-3
  [n ]
  (neg? n))

(función-neg?-1 -10)
(función-neg?-2 10)
(función-neg?-3 0)

;funcion nil?
(defn función-nil?-1
  [a]
  (nil? a))
(defn función-nil?-2
  [a]
  (nil? a))
(defn función-nil?-3
  [a]
  (nil? a))

(función-nil?-1 nil)
(función-nil?-2 [nil])
(función-nil?-3 [5 90 10])
;----------------------------------------------Fin sexto commit---------------------------------------
;----------------------------------------------Inicio Septimo Commit----------------------------------
;funcion not-empty
(defn función-not-empty-1
  [a]
  (not-empty a))

(defn función-not-empty-2
  [a ]
  (not-empty a))

(defn función-not-empty-3
  [a]
  (not-empty a))

(función-not-empty-1 [230 12])
(función-not-empty-2 '(50 10 60 90))
(función-not-empty-3 nil)

(defn función-nth-1
  [a b]
  (nth b a))
(defn función-nth-2
  [a b]
(nth b a))
(defn función-nth-3
  [a b]
(nth b a))

(función-nth-1 2 [10 20 30 40 50 60 70])
(función-nth-2 11 [10 20 true 40 50 false 60 70 80 "g" "x" "3" ])
(función-nth-3 5[10 20 30 40 50 60 70 80 90 ])

;funcion odd?
(defn función-odd?-1
  [n]
  (odd? n))

(defn función-odd?-2
  [[n]]
  (odd? (first [n])))

(defn función-odd?-3
  [n]
  (odd? n ))

(función-odd?-1 3)
(función-odd?-2 [20 30])
(función-odd?-3 10)

;funcion Partition
(defn función-partition-1
  [n a]
  (partition n (range a)))

(defn función-partition-2
  [a b]
  (partition a (range b)))

(defn función-partition-3
  [a b c]
  (partition a b (range c)))

(función-partition-1 5 10)
(función-partition-2 6 50)
(función-partition-3 3 7 40)

;funcion partition-all
(defn función-partition-all-1
  [a b]
  (partition-all a b))

(defn función-partition-all-2
  [a b]
  (partition-all a [b]))

(defn función-partition-all-3
  [a b c]
  (partition-all a b c))

(función-partition-all-1 3 [0 1 2 3 4 5 6 7 8 9])
(función-partition-all-2 5 [0 1 2 3 4 5 6 7 8 9])
(función-partition-all-3 1 3 [0 1 2 3 4 5 6 7 8 9])
;-----------------------------------------------Fin del Sexto Commit---------------------------
;-----------------------------------------------Inicio del septtimo Commit----------------------
;funcion peek
(defn función-peek-1
  [a ]
  (peek a ))

(defn función-peek-2
  [a]
  (peek a))

(defn función-peek-3
  [a]
  (peek a))
(función-peek-1 [10 30 false 90 60 "Prueba" 60])
(función-peek-2 [10 60 90 10 6 true])
(función-peek-3 ["Aloo" false \Q])

;funcion pop
(defn función-pop-1
  [a]
  (pop a))

(defn función-pop-2
  [a]
  (pop a))
(defn función-pop-3
  [a]
  (pop a))

(función-pop-1 [true false true \a])
(función-pop-2 [10 20 30 40])
(función-pop-3 '(1 6 100))

;funcion pos
(defn función-pos?-1
  [n]
  (pos? n))

(defn función-pos?-2
  [n]
  (pos? n))

(defn función-pos?-3
  [n]
  (pos? n))

(función-pos?-1 -200)
(función-pos?-2 (función-peek-1 [60 50 -10]))
(función-pos?-3 1)

;funcion range
(defn función-range-1
  [a]
  (range a))

(defn función-range-2
  [a b]
  (range a b))

(defn función-range-3
  [a b c]
  (range a b c))
(función-range-1 5)
(función-range-2 -1 10)
(función-range-3 5 10 1)
;-------------------------------------------Fin commit 7------------------------------------------
;-------------------------------------------Inicio commit 8---------------------------------------
; funcion rem
(defn función-rem-1
  [dividendo divisor]
  (rem dividendo divisor))
(defn función-rem-2
  [dividendo divisor]
  (rem dividendo divisor))
(defn función-rem-3
  [dividendo divisor]
  (rem dividendo divisor))

(función-rem-1 12 4)
(función-rem-2 100 7)
(función-rem-3 17 6)

(defn función-repeat-1
  [n]
  (repeat n))
(defn función-repeat-2
  [n d]
  (repeat n  d))
(defn función-repeat-3
  [n d]
  (repeat n  d))

(función-repeat-1 5)
(función-repeat-2 15 "x")
(función-repeat-3 12 #{\a \b})

;replace checar
;rest: Devuelve una secuencia posiblemente vacía de los elementos después del primero. 
(defn función-rest-1
  [c]
  (rest c))
(defn función-rest-2
  [c]
  (rest c))
(defn función-rest-3
  [c]
  (rest c))
(función-rest-1 [10 20 30 40 50 60])
(función-rest-2 (vector "Hola" "como" "estas" \?))
(función-rest-3 [])

(defn función-key-1
  [m k]
  (select-keys m k))
(defn función-key-2
  [m k]
  (select-keys m k))
(defn función-key-3
  [m k]
  (select-keys m k))

(función-key-1 {:17161210 "Oscar" :17161170 "Jennifer"} [:17161210 :17161170])
(función-key-2 {:a 52 :b 78 :c 120} [:a :b])
(función-key-3 [0 1 2] [2 3 4])

(defn funcíón-shuffle-1
  [c]
  (shuffle c)) 
(defn funcíón-shuffle-2
  [c n]
  (repeat n (shuffle c)))
(defn funcíón-shuffle-3
  [c]
  (shuffle c))

(funcíón-shuffle-1 [1 2 3 3 5])
(funcíón-shuffle-2 [1 7 9 6] 2)
(funcíón-shuffle-3 '(1 true false "Hello"))

(defn función-sort-1
  [e]
  (sort e))

(defn función-sort-2
  [e]
  (sort e))

(defn función-sort-3
  [e]
  (sort e))
(función-sort-1 [1 8 7 9 0 3])
(función-sort-2 {\q \p \a \b \c \s})
(función-sort-3 #{\a \b \q \c \0})

(defn funcíon-split-at-1
  [n c]
  (split-at n c))
(defn funcíon-split-at-2
  [n c]
  (split-at n c))
(defn funcíon-split-at-3
  [n c]
  (funcíon-split-at-2 n (función-sort-1 c)))

(funcíon-split-at-1 3 [1 2 5 9 0 9])
(funcíon-split-at-2 4 #{\a \z \q})
(funcíon-split-at-3 5 [7 9 0 6 19 21 36 5])

(defn función-str-1
  [n]
  (str n))
(defn función-str-2
  [e]
  (str e))
(defn función-str-3
  [e]
  (str e))

(función-str-1 78995)
(función-str-2 ["Juan" true 150 \R \@])
(función-str-3 true)

(defn función-subs-1
  [c i]
  (subs c i))

(defn función-subs-2
  [c s e]
  (subs c s e))

(defn función-subs-3
  [c s e]
  (subs (función-str-2 c) s e))

(función-subs-1 "Alooo!" 1)
(función-subs-2 "Aloo!" 2 4)
(función-subs-3 [1 3 1 6 3 7] 1 5)

(defn función-subvec-1
  [c s]
  (subvec c s))

(defn función-subvec-2
  [c s e]
  (subvec c s e))

(defn función-subvec-3
  [c s e]
  (subvec c s e))

(función-subvec-1 [1 9 7 6 9 4 6 3] 5)
(función-subvec-2 [1 9 7 6 9 4 6 3] 3 6)
(función-subvec-3 [[1 \b "Aloo" true] [7 false "Bye" \@] [\z 8 "..."]] 0 2)

(defn función-take-1
  [n c]
  (take n c))
(defn función-take-2
  [n c]
  (take n c))
(defn función-take-3
  [n c]
  (take n c))
(función-take-1 3 [\a \b \c \d \e \f])
(función-take-2 1 '("Aloo" "Assembler" "Foda"))
(función-take-1 0 {:1520 "Juanjo" :1151 "Alfredo"})
;--------------------------------------------------Fin del OCtavo Commit------------------------
;--------------------------------------------------Inicia Noveno-------------------------------

;funcion quote
(defn función-qout-1
  [a]
  (quot 10 a))

(defn función-qout-2
  [a b]
  (quot a b))

(defn función-qout-3
  [a [c]]
  (quot a (first [c])))

(función-qout-1 8)
(función-qout-2 95 10)
(función-qout-3 4 [1 2 3 4 5])

(defn función-true-?-1
  [c]
  (true? c))

(defn función-true-?-2
  [c]
  (true? c))
(defn función-true-?-3
  [c]
  (true? c))
(función-true-?-1 true)
(función-true-?-2 nil)
(función-true-?-3 [])

;-------------------------------------------------Fin Noveno ----------------------------------------
;-------------------------------------------------Inicia Decimo-------------------------------------
;
(defn función-zero-?-1
  [c]
  (zero? c))

(defn función-zero-?-2
  [c]
  (zero? c))
(defn función-zero-?-3
  [c]
  (zero? c))
(función-zero-?-1 -1)
(función-zero-?-2 0)
(función-zero-?-3 (- 5 5))

(defn función-val-1
  [a]
  (val a))

(defn función-val-2
  [a]
  (val a))

(defn función-val-3
  [a]
  (val a))

(función-val-1 (first {:17161158 "Maria" :16161357 "Juana"}))
(función-val-2 (first {:one :two}))
(función-val-3 (last {:a [10 9 8] :b [7 6 5] :c [4 3 2]}))

(defn función-vals-1
  [a]
  (vals a))

(defn función-vals-2
  [a]
  (vals a))

(defn función-vals-3
  [a]
  (vals a))

(función-vals-1 {:a 1 :b 2 :c 3})
(función-vals-2 {:one :two})
(función-vals-3 {:a '(10 50 true) :b [4 56 5] :c {:15 "Name" :16 "Tito"}})

;funcion zip map
(defn función-zipmap-1
  [a b]
  (zipmap a b))

(defn función-zipmap-2
  [a b]
  (zipmap a b))

(defn función-zipmap-3
  [a b]
  (zipmap a b))

(función-zipmap-1 [:a :b :c] [\a \b \c])
(función-zipmap-2 '(:a :b :c) '(10 20 30))
(función-zipmap-3 #{:a :b :c} #{true false \@ "Aloo"})

(defn función-replace-1
  [a b]
  (replace a b))

(defn función-replace-2
  [a b]
  (replace a b))

(defn función-replace-3
  [a b]
  (replace a b))

(función-replace-1 [1 2 5 1] [])
(función-replace-2 [54 9 51 20] [51 7 36 60])
(función-replace-3 ["Alooo"] ["Bye"])